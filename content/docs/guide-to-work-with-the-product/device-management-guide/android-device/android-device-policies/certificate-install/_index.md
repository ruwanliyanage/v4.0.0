---
bookCollapseSection: true
weight: 7
---
# Certificate Install 

{{< hint info >}}
<b> <a href ="{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/android-device/android-device-policies/#add-a-policy"> Add policy </a></b>in Policy description page gives a brief description how a policy is added to an Android device.
{{< /hint >}}

This configurations can be used to install certificate on an Android device.

<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
        </tr>
        <tr>
            <td><strong>Certificate name</strong></td>
            <td>The file name of the enclosed certificate.
            </td>
        </tr>
        <tr>
            <td><strong>Certificate file</strong></td>
            <td>The base64 representation of the payload with a line length of 52.</td>
        </tr>
        <tr>
            <td><strong>Certificate type</strong></td>
            <td>Certificate should be a DER-encoded X.509 SSL certificate in format of .crt or .cer</td>
        </tr>
    </tbody>
</table>

{{< hint info >}}
<b> <a href = "{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/android-device/android-device
-policies/#publish-a-policy"> Publish a policy </a></b>in Policy description page gives a brief description how a policy is published.
 {{< /hint >}}
