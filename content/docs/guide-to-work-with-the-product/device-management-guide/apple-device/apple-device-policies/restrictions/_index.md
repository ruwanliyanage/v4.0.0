---
bookCollapseSection: true
weight: 2
---

# Restrictions


{{< hint info >}}
<b> <a href ="{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/#add-a-policy"> Add policy </a></b>in Policy description page gives a brief description how a policy is added to an Android device.
{{< /hint >}}

These configurations can be used to restrict apps, device features and media content available on an iOS device. Once this configuration profile is installed on a device, corresponding users will not be able to modify these settings on their devices.

<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
        <td colspan="2"><strong><center>Restrictions on mac OS and iOS device</center></strong></td>
        </tr>
        <tr>
            <td><strong>Allow Siri</strong></td>
            <td>When false, disables Siri. Defaults to true.</td>
        </tr>
        <tr>
            <td><strong>Allow use of camera</strong></td>
            <td>Having this checked would enable Usage of phone camera in the device
            </td>
        </tr>
        <tr>
            <td><strong>Allow iCloud documents and data</strong><br> <i>[This key is deprecated 
            on unsupervised devices.]</i></td>
            <td>Having this checked would enable syncing iCloud documents and data in the device.
             This is deprecated on unsupervised devices<br>Available in iOS 5.0 and later and in
              macOS 10.11and later.</td>
        </tr>
        <tr>
            <td><strong>Allow iCloud keychain</strong></td>
            <td> When false, disables iCloud keychain
                synchronization. Default is true.<br>Available in iOS 7.0 and later and macOS 10
                .12 and later.</td>
        </tr>
        <tr>
            <td><strong>Allow fingerprint for unlock</strong></td>
            <td>If false, prevents Touch ID from unlocking a device.<br>Available in iOS 7 and later and in macOS 10.12.4
                                                                        and later.</td>
        </tr>
        <tr>
            <td><strong>Allow in-app purchase</strong></td>
            <td>Having this checked would allow in-app purchase in the device.</td>
        </tr>
        <tr>
            <td><strong>Allow screenshots</strong></td>
            <td>If set to false, users canʼt save a screenshot of the
                display and are prevented from capturing a screen recording; it
                also prevents the Classroom app from observing remote screens.
        </tr>
        <tr>
            <td><strong>Enable AutoFill</strong></td>
            <td>When false, Safari auto-fill is disabled. Defaults to true.</td>
        </tr>
        <tr>
            <td><strong>Allow voice dialing while device is locked</strong></td>
            <td>When false, disables voice dialing if the device is locked with a passcode. 
            Default is true.
            </td>
        </tr>
        <tr>
            <td><strong>Force encrypting all backups</strong></td>
            <td>Having this checked would force encrypting all backups.</td>
        </tr>
        <tr><td><strong>Allow managed apps to store data in iCloud</strong></td>
            <td>If set to false, prevents managed applications from using iCloud sync.</td>
        </tr>
        <tr>
            <td><strong>Allow Activity Continuation</strong></td>
            <td>If set to false, Activity Continuation will be disabled. Defaults to true.
            </td>
        </tr>
        <tr>
            <td><strong>Allow backup of enterprise books</strong></td>
            <td> If set to false, Enterprise books will not be backed up. Defaults to true</td>
        </tr>
        <tr>
            <td><strong>Allow enterprise books data sync</strong></td>
            <td>If set to false, Enterprise books notes and highlights will not be
                 synced. Defaults to true.</td>
        </tr>
        <tr>
            <td><strong>Allow cloud photo library</strong></td>
            <td>If set to false, disables iCloud Photo Library. Any photos not
                fully downloaded from iCloud Photo Library to the device will be
                removed from local storage.
            </td>
        </tr>
        <tr>
            <td><strong>Allow remote screen observation</strong></td>
            <td> If set to false, remote screen observation by the Classroom app
                 is disabled. Defaults to true.
                 This key should be nested beneath allowScreenShot as a
                 sub-restriction. If allowScreenShot is set to false, it also
                <br>Available in iOS 9.3 and macOS 10.14.4 and later.
            </td>
        </tr>
        <tr>
            <td><strong>Allow adding Game Center friends</strong><br>[This key is deprecated on unsupervised devices.]</td>
            <td>When false, prohibits adding friends to Game Center.
                This key is deprecated on unsupervised devices.</td>
        </tr>
        <tr>
            <td><strong>Allow Siri to query user-generated content from web</strong></td>
            <td> Supervised only. When false, prevents Siri from
                querying user-generated content from the web.<br>Available in iOS 7 and later.
            </td>
        </tr>
        <tr>
            <td><strong>Allow video conferencing </strong><br>[This key is deprecated on unsupervised devices.]</td>
            <td>When false, disables video conferencing. This key is deprecated on unsupervised 
            devices</td>
        </tr>
        <tr>
            <td><strong>Allow Safari</strong><br>[This key is deprecated on unsupervised devices.]</td>
            <td>When false, the Safari web browser application is
                disabled and its icon removed from the Home screen. This also
                prevents users from opening web clips. This key is deprecated on
                unsupervised devices.
            </td>
        </tr>
        <tr>
            <td><strong>Allow multiplayer gaming</strong><br>[This key is deprecated on unsupervised devices.]</td>
            <td>When false, prohibits multiplayer gaming. This key is deprecated on unsupervised 
            devices.</td>
        </tr>
        <tr>
            <td><strong>Allow use of iTunes Store</strong></td>
            <td>When false, the iTunes Music Store is disabled and its
                icon is removed from the Home screen. Users cannot preview,
                purchase, or download content. This key is deprecated on
                unsupervised devices.</td>
        </tr>
        <tr>
            <td colspan="2"><strong><center>Restrictions on iOS device</center></strong></td>
        </tr>
        <tr>
            <td><strong>Allow Siri while device is locked</strong></td>
            <td>When false, the user is unable to use Siri when the
                device is locked. Defaults to true. This restriction is ignored if
                the device does not have a passcode set.
            </td>
        </tr>
        <tr>
            <td><strong>Allow removing apps</strong><br>[This key is deprecated on unsupervised devices
            .]</td>
            <td> When false, disables removal of apps from iOS
                device. This key is deprecated on unsupervised devices.
            </td>
        </tr>
        <tr>
            <td><strong>Allow iCloud backup</strong></td>
            <td> When false, disables backing up the device to iCloud.
            </td>
        </tr>
        <tr>
            <td><strong>Allow diagnostic submission</strong></td>
            <td> When false, this prevents the device from
                automatically submitting diagnostic reports to Apple. Defaults to
                true.
                <br>Available only in iOS 6.0 and later.
            </td>
        </tr>
        <tr>
            <td><strong>Allow explicit content </strong><br>[This key is deprecated on 
            unsupervised devices
            .]</td>
            <td>When false, explicit music or video content
                purchased from the iTunes Store is hidden. Explicit content is
                marked as such by content providers, such as record labels,
                when sold through the iTunes Store. This key is deprecated on
                unsupervised devices.<br>Available in iOS and in tvOS 11.3 and later
            </td>
        </tr>
        <tr>
            <td><strong>Allow global background fetch when roaming</strong></td>
            <td>When false, disables global background fetch activity
                when an iOS phone is roaming.</td>
        </tr>
                <tr>
                    <td><strong>Show Notifications Center in lock screen</strong></td>
                    <td>If set to false, the Notifications history view on the
                        lock screen is disabled and users canʼt view past notifications.
                        Though, when the device is locked, the user will still be able to
                        view notifications when they arrive.<br>Available only in iOS 7.0 and later.</td>
                </tr>
                <tr>
                    <td><strong>Show Today view in lock screen</strong></td>
                    <td>If set to false, the Today view in Notification Center
                        on the lock screen is disabled.<br>Available only in iOS 7.0 and later.</td>
                </tr>
                <tr>
                    <td><strong>Allow documents from managed sources in unmanaged destinations</strong></td>
                    <td> If false, documents in managed apps and accounts
                        only open in other managed apps and accounts. Default is true.<br>Available only in iOS 7.0 and later</td>
                </tr>
                <tr>
                    <td><strong>Allow documents from unmanaged sources in managed destinations</strong></td>
                    <td>If set to false, documents in unmanaged apps and
                        accounts will only open in other unmanaged apps and accounts.
                        Default is true.<br>Available only in iOS 7.0 and later.</td>
                </tr>
                <tr>
                    <td><strong>Show Passbook notifications in lock screen</strong></td>
                    <td>If set to false, Passbook notifications will not be
                        shown on the lock screen.This will default to true.<br>Available in iOS 6.0 and later.</td>
                </tr>
                <tr>
                    <td><strong>Allow Photo Stream</strong></td>
                    <td>When false, disables Photo Stream.<br>Available in iOS 5.0 and later.</td>
                </tr>
                <tr>
                    <td><strong>Force Fraud warning</strong></td>
                    <td> When true, Safari fraud warning is enabled. Defaults
                        to false<br>Available in iOS 4.0 and later.</td>
                </tr>
                <tr>
                    <td><strong>Enable Javascript</strong></td>
                    <td>When false, Safari will not execute JavaScript.
                        Defaults to true.<br>Available in iOS 4.0 and later.</td>
                </tr>
                <tr>
                    <td><strong>Enable Pop-ups</strong></td>
                    <td>When false, Safari will not allow pop-up tabs.
                        Defaults to true.<br>Available in iOS 4.0 and later.
                    </td>
                </tr>
                <tr>
                    <td><strong>Accept cookies</strong></td>
                    <td>Determines conditions under which the device will
                        accept cookies.
                        The user facing settings changed in iOS 11, though the possible
                        values remain the same:<br>
                        • 0: Prevent Cross-Site Tracking and Block All Cookies are
                        enabled and the user canʼt disable either setting.<br>
                        • 1 or 1.5: Prevent Cross-Site Tracking is enabled and the
                        user canʼt disable it. Block All Cookies is not enabled,
                        though the user can enable it.<br>
                        • 2: Prevent Cross-Site Tracking is enabled and Block All
                        Cookies is not enabled. The user can toggle either setting.
                        (Default)<br>
                        These are the allowed values and settings in iOS 10 and earlier:<br>
                        • 0: Never<br>
                        • 1: Allow from current website only<br>
                        • 1.5: Allow from websites visited (Available in iOS 8.0 and
                        later); enter ’<real>1.5</real>’<br>
                        • 2: Always (Default)<br>
                        In iOS 10 and earlier, users can always pick an option that is more
                        restrictive than the payload policy, but not a less restrictive policy.
                        For example, with a payload value of 1.5, a user could switch to
                        Never, but not Always Allow.</td>
                </tr>
                <tr><td><strong>Allow Shared Photo Stream</strong></td>
                    <td> If set to false, Shared Photo Stream will be disabled.This will default 
                    to true.<br>Available in iOS 6.0 and later.</td>
                </tr>
                <tr>
                    <td><strong>Allow untrusted TLS prompt</strong></td>
                    <td>When false, automatically rejects untrusted HTTPS
                        certificates without prompting the user.<br>Available in iOS 5.0 and later.</td>
                </tr>
                <tr>
                    <td><strong>Require iTunes store password for all purchases</strong></td>
                    <td>When true, forces user to enter their iTunes password
                        for each transaction<br>Available in iOS 5.0 and later.</td>
                </tr>
                <tr>
                    <td><strong>Limit ad tracking</strong></td>
                    <td>If true, limits ad tracking. Default is false<br>Available only in iOS 7.0 and later</td>
                </tr>
                <tr>
                    <td><strong>Force a pairing password for Airplay outgoing requests</strong></td>
                    <td>If set to true, forces all devices receiving AirPlay
                        requests from this device to use a pairing password. Default is
                        false.<br>Available only in iOS 7.1 and later.</td>
                </tr>
                <tr>
                    <td><strong>Force air drop unmanaged</strong></td>
                    <td>If set to true, causes AirDrop to be considered an unmanaged drop target.
                     Defaults to false.
                        <br>Available in iOS 9.0 and later.
                    </td>
                </tr>
                <tr>
                    <td><strong>Force watch wrist detection</strong></td>
                    <td>If set to true, a paired Apple Watch will be forced to use Wrist
                        Detection. Defaults to false.<br>Available in iOS 8.2 and later.</td>
                </tr>
                <tr>
                    <td><strong>Allow over-the-air PKI updates</strong></td>
                    <td>If false, over-the-air PKI updates are disabled.
                        Setting this restriction to false does not disable CRL and OCSP
                        checks. Default is true.<br>Available only in iOS 7.0 and later.
                    </td>
                </tr>
                <tr>
                    <td><strong>Ratings region</strong></td>
                    <td>This 2-letter key is used by profile tools to display the proper
                        ratings for given region. It is not recognized or reported by the
                        client.<br>
                        Possible values:<br>
                        • au: Australia<br>
                        • ca: Canada<br>
                        • fr: France<br>
                        • de: Germany<br>
                        • ie: Ireland<br>
                        • jp: Japan<br>
                        • nz: New Zealand<br>
                        • gb: United Kingdom<br>
                        • us: United States<br>
                        Available in iOS and tvOS 11.3 and later</td>
                </tr>
                <tr>
                    <td><strong>Allow content ratings</strong><br>Having this checked would allow to set the maximum allowed ratings</td>
                    <td colspan="2">
                                    <table style="width: 100%;">
                                        <tbody>
                                            <tr>
                                                <td>Allowed content ratings for movies</td>
                                                <td> This value defines the maximum level of movie content that is
                                                    allowed on the device.
                                                    Possible values (with the US description of the rating level):
                                                    • 1000: All
                                                    • 500: NC-17
                                                    • 400: R
                                                    • 300: PG-13
                                                    • 200: PG
                                                    • 100: G
                                                    • 0: None<br>Available only in iOS and tvOS 11.3 and later</td>
                                            </tr>
                                            <tr>
                                                <td>Allowed content ratings for TV shows</td>
                                                <td>This value defines the maximum level of TV content that is
                                                    allowed on the device.
                                                    Possible values (with the US description of the rating level):
                                                    • 1000: All
                                                    • 600: TV-MA
                                                    • 500: TV-14
                                                    • 400: TV-PG
                                                    • 300: TV-G
                                                    • 200: TV-Y7
                                                    • 100: TV-Y
                                                    • 0: None<br>Available only in iOS and tvOS 11.3 and later.</td>
                                            </tr>
                                            <tr>
                                                <td>Allowed content ratings for apps</td>
                                                <td> This value defines the maximum level of app content that is
                                                    allowed on the device.
                                                    Possible values (with the US description of the rating level):
                                                    • 1000: All
                                                    • 600: 17+
                                                    • 300: 12+
                                                    • 200: 9+
                                                    • 100: 4+
                                                    • 0: None<br>Available only in iOS 5 and tvOS
                                                     11.3 and later.</td>
                                        </tbody>
                                    </table>
                                </td>
                </tr>
                <tr>
                    <td><strong>Allow enterprise app trust</td>
                    <td>If set to false removes the Trust Enterprise Developer button in
                        Settings->General->Profiles & Device Management, preventing
                        apps from being provisioned by universal provisioning profiles.
                        This restriction applies to free developer accounts but it does not
                        apply to enterprise app developers who are trusted because their
                        apps were pushed via MDM, nor does it revoke previously
                        granted trust. Defaults to true.<br>Available in iOS 9.0 and later.</td>
                </tr>
                <tr>
                    <td><strong>Show Control Center in lock screen</strong></td>
                    <td>If false, prevents Control Center from appearing on
                        the Lock screen.<br>Available in iOS 7 and later.</td>
                </tr>
                <tr>
                    <td><strong>Read unmanaged apps from managed contact accounts.</strong></td>
                    <td>If set to true, unmanaged apps can read from
                        managed contacts accounts. Defaults to false.
                        if allowOpenFromManagedToUnmanaged is true, this
                        restriction has no effect.
                        A payload that sets this to true must be installed via MDM.<br>Available only in iOS 12.0 and later</td>
                </tr>
                <tr>
                    <td><strong>Allow Siri while device is locked</strong></td>
                    <td>When false, the user is unable to use Siri when the
                        device is locked. Defaults to true. This restriction is ignored if
                        the device does not have a passcode set.
                    </td>
                </tr>
                <tr>
                    <td><strong>Allow removing apps</strong><br>[This key is deprecated on unsupervised devices
                    .]</td>
                    <td> When false, disables removal of apps from iOS
                        device. This key is deprecated on unsupervised devices.
                    </td>
                </tr>
                <tr>
                    <td><strong>Allow iCloud backup</strong></td>
                    <td> When false, disables backing up the device to iCloud.
                    </td>
                </tr>
                <tr>
                    <td><strong>Allow diagnostic submission</strong></td>
                    <td> When false, this prevents the device from
                        automatically submitting diagnostic reports to Apple. Defaults to
                        true.
                        <br>Available only in iOS 6.0 and later.
                    </td>
                </tr>
                <tr>
                    <td><strong>Allow explicit content </strong><br>[This key is deprecated on 
                    unsupervised devices
                    .]</td>
                    <td>When false, explicit music or video content
                        purchased from the iTunes Store is hidden. Explicit content is
                        marked as such by content providers, such as record labels,
                        when sold through the iTunes Store. This key is deprecated on
                        unsupervised devices.<br>Available in iOS and in tvOS 11.3 and later
                    </td>
                </tr>
                <tr>
                    <td><strong>Allow global background fetch when roaming</strong></td>
                    <td>When false, disables global background fetch activity
                        when an iOS phone is roaming.</td>
                </tr>
    </tbody>
</table>

{{< hint info >}}
<b> <a href = "{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/#publish-a-policy"> Publish a policy </a></b>in Policy description page gives a brief description how a policy is published.
 {{< /hint >}}